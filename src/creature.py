from abc import ABC, abstractmethod
import dnd
import math
import util

class Creature(ABC):
    """
    Describes a Creature, being either a player Character or a Monster.
    """

    def __init__(self, name: str, max_hitpoints = 1, proficiency_bonus = 2) -> None:
        """
        Any optional attributes not provided must be initialized later.
        """

        super().__init__()

        # Creature name
        self._name = name

        # Maximum hitpoints
        self._max_hitpoints = 1
        self.set_max_hitpoints(max_hitpoints)
        # Current hitpoints
        self._hitpoints = 1
        self.set_hitpoints(self._max_hitpoints)

        # Dict of ability: score
        self._abilities = {}
        # Dict of of skill: bool, True if proficient
        self._skills = {}
        # Dict of saving throw: bool, True if proficient
        self._saving_throws = {}

        # List of proficiencies
        self._proficiencies = []
        # Proficiency bonus (based on level or CR)
        self._proficiency_bonus = proficiency_bonus

        if (not self._abilities):
            for ability in dnd.get_abilities():
                self._abilities[ability["index"]] = 0

        if (not self._saving_throws):
            for ability in dnd.get_abilities():
                self._saving_throws[ability["index"]] = 0

        if (not self._skills):
            for skill in dnd.get_skills():
                self._skills[skill["index"]] = 0


    def set_max_hitpoints(self, new_max_hitpoints: int) -> int:
        """
        Set the maximum amount of hitpoints the Creature can have.
        """

        self._max_hitpoints = max(1, new_max_hitpoints)
        return self._max_hitpoints


    def set_hitpoints(self, new_hitpoints: int) -> int:
        """
        Set current hitpoints to the given value, returning the amount gained.
        """

        old_hitpoints = self._hitpoints
        self._hitpoints = min(self._max_hitpoints, new_hitpoints)
        return self._hitpoints - old_hitpoints


    def heal(self, heal_amount: int) -> int:
        """
        Heal for the given amount, returning the amount gained.
        """

        return self.set_hitpoints(self._hitpoints + heal_amount)


    def damage(self, damage_amount: int) -> bool:
        """
        Take the given amount of damage, returning True if it kills the Creature.
        """

        self.set_hitpoints(self._hitpoints - damage_amount)
        return self._hitpoints <= 0


    def add_proficiency(self, proficiency_index: str) -> dict:
        """
        Add a proficiency. If its a skill or saving throw, add it as proficient.
        """

        proficiency = dnd.get_proficiency(proficiency_index)

        if (proficiency["type"].lower() == "skills"):
            self._skills[proficiency["reference"]["index"]] = 1

        elif (proficiency["type"].lower() == "saving throws"):
            self._saving_throws[proficiency["reference"]["index"]] = 1

        self._proficiencies.append(proficiency)
        return proficiency


    def ability_modifier(self, ability_index: str) -> int:
        """Calculate the ability modifier for the given ability."""

        return math.floor((self._abilities[ability_index] - 10) / 2)


    def ability_check(self, ability_index: str) -> int:
        """Perform an ability check for the given ability."""

        roll = dnd.roll_sum("1d20") + self.ability_modifier(ability_index)
        return roll


    def saving_throw(self, ability_index: str) -> int:
        """Perform a saving throw for the given ability."""

        roll = dnd.roll_sum("1d20") + self.ability_modifier(ability_index)
        if (self._saving_throws[ability_index]):
            roll += max(self._saving_throws[ability_index], self._proficiency_bonus)

        return roll


    def skill_check(self, skill_index: str) -> int:
        """Perform a skill check for the given skill."""

        skill_ability = dnd.get_skill(skill_index)["ability_score"]["index"]
        roll = dnd.roll_sum("1d20") + self.ability_modifier(skill_ability)
        if (self._skills[skill_index]):
            roll += max(self._skills[skill_index], self._proficiency_bonus)

        return roll


    def hitpoint_bar(self) -> str:
        """Get the hitpoint bar for the Creature, with 1 heart representing 1/10 hitpoints."""

        full = util.emojis["health"]["full"]
        empty = util.emojis["health"]["empty"]
        full_count = max(0, self._hitpoints)
        empty_count = self._max_hitpoints - full_count

        if (self._max_hitpoints > 10):
            full_count = max(0, math.ceil((self._hitpoints / self._max_hitpoints) * 10))
            empty_count = 10 - full_count

        return "{}{} {}/{}".format(
            full * full_count,
            empty * empty_count,
            self._hitpoints,
            self._max_hitpoints
        )


    @abstractmethod
    def armor_class(self, armor: dict = None) -> int:
        """
        Get the Armor Class of the Creature.
        If `armor` is provided, AC is calculated as if using it (only on a Character).
        """
        pass


    @abstractmethod
    def attack(self, other: "Creature") -> dnd.AttackResult:
        """
        Attack the `other` Creature, returning the damage to be done.
        This does not actually inflict damage, only calculates an attack against `other`.
        """
        pass


    @abstractmethod
    def emoji(self) -> str:
        """
        Get the emoji associated with the Creature.
        """
        pass


    @abstractmethod
    def bio(self) -> str:
        """
        Get a long description of the Creature. Generally seen when directly interacting with
        the Creature.
        """
        pass


    @abstractmethod
    def short_bio(self) -> str:
        """
        Get a short description of the Creature. Generally seen as a preview before interacting
        with the Creature.
        """
        pass
