import util
import dnd
from character import Character
from monster import Monster
import client
import lang
import random
from datetime import datetime, timezone
from enum import Enum
import math
import pickle
import os

class State(Enum):

    END = 0
    BUILD = 1
    ADVENTURE = 2
    FIGHT = 3
    TOWN = 4
    SHOP = 5


# TODO: the stretch goal is for each of the functions in this class
# to be separated into node types that can be populated with behavior
# via a toml configuration
class Campaign:
    """
    A single Campaign, spanning the lifetime of a single character.
    A new Campaign is created upon character death.
    """

    def __init__(self, id: int = 1, version: str = "0.0.0") -> None:
        """
        Create a new Campaign.
        `id`: ID of this Campaign.
        `callback`: called when the Campaign ends.
        """

        self._id: int = id
        self._character: Character = None
        self._monster: Monster = None
        self._start_time: datetime = None
        self._state: State = State.BUILD
        self._version = version


    def start(self, result = ""):

        util.info(f"STARTING CAMPAIGN #{self._id}")

        while (self._state != State.END):

            if (self._state == State.BUILD):
                result = self.build(result)

            elif (self._state == State.ADVENTURE):
                result = self.adventure(result)

            elif (self._state == State.FIGHT):
                result = self.fight(result)

            elif (self._state == State.TOWN):
                result = self.town(result)

            elif (self._state == State.SHOP):
                result = self.shop(result)

            if (self._character):
                client.update_profile(self.bio())

            util.info(result)
            self.save()

        result = self.end(result)


    def save(self):
        """Save Campaign object to file."""

        with open(os.path.join(util.root, "data/save"), "wb") as file:
            pickle.dump({
                "campaign": self,
                "status_chain": client.status_chain
            }, file)


    @staticmethod
    def load():
        """Load Campaign object from file."""

        with open(os.path.join(util.root, "data/save"), "rb") as file:
            data = pickle.load(file)
            # Temporarily account for old save methods
            if (isinstance(data, Campaign)):
                return data
            else:
                campaign: Campaign = None
                for key in data:
                    match key:
                        case "campaign":
                            campaign = data[key]

                        case "status_chain":
                            client.status_chain = data[key]

                        case _:
                            util.error("Unexpected data key.")
                return campaign


    def build(self, last_result = "") -> str:

        # Generate character options
        util.info("Creating character options")
        characters: list[Character] = []
        followers = client.get_followers()
        followers_const = followers.copy()
        for _ in range(client.MAX_POLL_SIZE):
            name = ""
            # choose a unique random name from follower list if available
            while (len(name) < 2 and followers):
                choice = random.choice(followers)
                followers.remove(choice)
                name = choice.split(" ")[0]
                name = "".join([c for c in name if c.isalpha()])

            if (not name):
                name = random.choice(followers_const).split(" ")[0]
                name = "".join([c for c in name if c.isalpha()])
            characters.append(Character(name))

        # Poll
        util.info("Polling for character selection...")
        titles = [f"{c.emoji()} {c.title()}" for c in characters]

        prompt = f"{util.emojis['campaign']['id']} Campaign {self._id} #dnd\n\n"
        prompt += lang.create.prompt.choose()
        status = client.post_poll(prompt, last_result, titles, False)
        result = client.poll_result(status["id"], False)

        # Set character
        self._character = characters[result]
        self._character.init()
        self._start_time = datetime.now(timezone.utc)
        client.update_profile(self.bio())

        self._state = State.TOWN
        return lang.create.result.choose(self._character._name)


    def fight(self, last_result = "") -> str:

        util.info(f"Fighting {self._monster._name}")

        options = [
            "{} Attack!".format(util.emojis["prompts"]["fight"]),
            "{} Run!".format(util.emojis["prompts"]["run"])
        ]
        status = client.post_poll(self._monster.bio() + f"\n{self._monster._image_url}", last_result, options)
        result = client.poll_result(status["id"])
        attack_summary = []

        match result:
            # Attack
            case 0:
                character_attack = self._character.attack(self._monster)
                monster_attack = self._monster.attack(self._character)

                # Reroll until someone hits
                while (character_attack.attack_type.value <= 0 and monster_attack.attack_type.value <= 0):
                    character_attack = self._character.attack(self._monster)
                    monster_attack = self._monster.attack(self._character)

                if (character_attack.attack_type == dnd.AttackType.CRIT_SUCCESS):
                    attack_summary.append(lang.attack_character.result.choose(
                        character_attack.name,
                        self._monster._name,
                        character_attack.damage,
                        option = "crit_success"
                    ))
                elif (character_attack.attack_type == dnd.AttackType.NORMAL):
                    attack_summary.append(lang.attack_character.result.choose(
                        character_attack.name,
                        self._monster._name,
                        character_attack.damage
                    ))

                # Attack the monster
                if (self._monster.damage(character_attack.damage)):

                    self._character._stats["kills"] += 1
                    reward_xp = self._monster._xp
                    reward_gold = math.ceil(self._monster._challenge_rating)
                    self._character.add_experience(reward_xp)
                    self._character.earn(reward_gold)

                    attack_summary.append(lang.death_monster.result.choose(self._monster._name))
                    attack_summary.append(lang.reward.choose("{}{} and {}{}".format(
                        reward_gold,
                        util.emojis["equipment"]["gold"],
                        reward_xp,
                        util.emojis["experience"]["xp"]
                    )))
                    self._state = State.ADVENTURE
                    self._monster = None
                    return "\n".join(attack_summary)

                # Attack the player
                excess_damage = monster_attack.damage - self._character._hitpoints
                self._state = State.FIGHT

                if (monster_attack.attack_type == dnd.AttackType.CRIT_SUCCESS):
                    attack_summary.append(lang.attack_monster.result.choose(
                        self._monster._name,
                        monster_attack.name,
                        monster_attack.damage,
                        option = "crit_success"
                    ))
                elif (monster_attack.attack_type == dnd.AttackType.NORMAL):
                    attack_summary.append(lang.attack_monster.result.choose(
                        self._monster._name,
                        monster_attack.name,
                        monster_attack.damage
                    ))

                if (self._character.damage(monster_attack.damage)):
                    if (excess_damage >= self._character._max_hitpoints):
                        attack_summary.append(lang.death_character.result.choose(option = "instant"))
                        self._state = State.END
                    elif (self._character.death_saving_throws()):
                        attack_summary.append(lang.death_character.result.choose(option = "unconscious"))
                        self._state = State.FIGHT
                    else:
                        attack_summary.append(lang.death_character.result.choose())
                        self._state = State.END

                return "\n".join(attack_summary)

            # Run
            case 1:
                monster_attack = self._monster.attack(self._character)
                excess_damage = monster_attack.damage - self._character._hitpoints
                self._state = State.ADVENTURE

                if (monster_attack.damage > 0):
                    attack_summary.append(lang.attack_monster.result.choose(self._monster._name, monster_attack.name, monster_attack.damage))
                else:
                    attack_summary.append(lang.run_character.result.choose())

                if (self._character.damage(monster_attack.damage)):
                    if (excess_damage >= self._character._max_hitpoints):
                        attack_summary.append(lang.death_character.result.choose(option = "instant"))
                        self._state = State.END
                    elif (self._character.death_saving_throws()):
                        attack_summary.append(lang.death_character.result.choose(option = "unconscious"))
                        self._state = State.ADVENTURE
                    else:
                        attack_summary.append(lang.death_character.result.choose())
                        self._state = State.END

                self._monster = None
                return "\n".join(attack_summary)

            case _:
                raise IndexError("Polled result is greater than number of options given.")


    def adventure(self, last_result = "") -> str:

        # Generate a monster
        self._monster = Monster(dnd.get_monster_lower(self._character._level / 3))

        # Poll
        options = [
            "{} Fight!".format(util.emojis["prompts"]["fight"]),
            "{} Avoid".format(util.emojis["prompts"]["stop"]),
            "{} Short Rest [{}{}] (+{} -{})".format(
                util.emojis["prompts"]["short_rest"],
                self._character._hit_dice,
                util.emojis["equipment"]["dice"],
                util.emojis["health"]["full"],
                util.emojis["equipment"]["dice"]
            ),
            "{} Go to Town".format(util.emojis["prompts"]["town"])
        ]
        status = client.post_poll(lang.fight.prompt.choose(self._monster.short_bio()) + f"\n{self._monster._image_url}", last_result, options)
        result = client.poll_result(status["id"])

        match result:
            # Fight
            case 0:
                self._state = State.FIGHT
                return lang.fight.result.choose(self._monster._name)

            # Avoid
            case 1:
                # TODO: add a stealth check
                self._state = State.ADVENTURE
                return lang.avoid.result.choose(self._monster._name)

            # Short Rest
            case 2:
                # TODO: make it clear you lose hit dice on this option
                regained = self._character.short_rest()
                self._state = State.ADVENTURE
                if (regained > 0):
                    return lang.rest_short.result.choose(regained)
                else:
                    return "You have no hit dice remaining."

            # Go to Town
            case 3:
                self._state = State.TOWN
                return lang.town.result.choose()

            case _:
                raise IndexError("Polled result is greater than number of options given.")


    def town(self, last_result = "") -> str:

        options = [
            "{} Adventure!".format(util.emojis["prompts"]["adventure"]),
            "{} Shop".format(util.emojis["prompts"]["shop"]),
            "{} Long Rest (+{} +{})".format(util.emojis["prompts"]["long_rest"], util.emojis["health"]["full"], util.emojis["equipment"]["dice"])
        ]
        status = client.post_poll(lang.town.prompt.choose(), last_result, options)
        result = client.poll_result(status["id"])

        match result:
            # Adventure
            case 0:
                self._state = State.ADVENTURE
                return lang.adventure.result.choose()

            # Shop
            case 1:
                self._state = State.SHOP
                return lang.shop.result.choose()

            # Long Rest
            case 2:
                self._state = State.TOWN
                regained = self._character.long_rest()
                return lang.rest_long.result.choose(regained)

            case _:
                raise IndexError("Polled result is greater than number of options given.")


    def shop(self, last_result = "") -> str:

        options = []
        equipment_options = []
        weapon_options = self._character.proficient_weapons()
        armor_options = self._character.proficient_armor()

        shop_size = client.MAX_POLL_SIZE - 1
        weapon_count = 0
        if (not armor_options):
            weapon_count = min(len(weapon_options), shop_size)
        else:
            weapon_count = min(len(weapon_options), random.randint(1, math.ceil(shop_size / 2)))
        armor_count = min(len(armor_options), shop_size - weapon_count)

        for _ in range(weapon_count):
            weapon = None
            cost = 0
            while ((not weapon or weapon == self._character._weapon) and weapon_options):
                weapon = random.choice(weapon_options)
                weapon_options.remove(weapon)
                cost = dnd.exchange(weapon["cost"]["unit"], weapon["cost"]["quantity"])
                if (cost > self._character._gp):
                    weapon = None

            if (weapon):
                modifier = self._character.weapon_modifier()
                equipment_options.append(weapon)
                # Dagger [1d4, +2]
                options.append(
                    "{} {} [{}, {}{}] (-{} {})".format(
                        util.emojis["equipment"]["weapon"],
                        weapon["name"],
                        weapon["damage"]["damage_dice"],
                        "+" if modifier >= 0 else "-",
                        abs(modifier),
                        cost,
                        util.emojis["equipment"]["gold"]
                    )
                )

        for _ in range(armor_count):
            armor = None
            cost = 0
            while ((not armor or armor == self._character._armor) and armor_options):
                armor = random.choice(armor_options)
                armor_options.remove(armor)
                cost = dnd.exchange(armor["cost"]["unit"], armor["cost"]["quantity"])
                if (cost > self._character._gp):
                    armor = None

            if (armor):
                equipment_options.append(armor)
                options.append(
                    "{} {} [{}] (-{} {})".format(
                        util.emojis['equipment']['armor'],
                        armor["name"],
                        self._character.armor_class(armor),
                        cost,
                        util.emojis["equipment"]["gold"]
                    )
                )

        if (not options):
            self._state = State.TOWN
            return "The shop is empty."

        options.append("{} Back to Town".format(util.emojis["prompts"]["back"]))
        status = client.post_poll(lang.shop.prompt.choose(), last_result, options)
        result = client.poll_result(status["id"])

        # Back to town
        if (result == len(options) - 1):
            self._state = State.TOWN
            return "You left the shop."
        # Buy
        else:
            choice = equipment_options[result]
            self._character.equip(choice)
            self._character.spend(dnd.exchange(choice["cost"]["unit"], choice["cost"]["quantity"]))
            self._state = State.TOWN
            return lang.purchase.result.choose(choice["name"])


    def end(self, last_result: str = "") -> None:

        if (not self._character):
            return

        util.info("Character died")
        delta = datetime.now(timezone.utc) - self._start_time
        bio = self._character.short_bio()
        bio += " {} {}".format(util.get_clock(delta.total_seconds()), util.format_duration(delta.total_seconds()))
        client.post_status(bio, last_result)


    def bio(self) -> str:

        delta = datetime.now(timezone.utc) - self._start_time
        return "{}\n\n{} {} {} {}".format(
            self._character.bio(),
            util.emojis["campaign"]["id"],
            self._id,
            util.get_clock(delta.total_seconds()),
            util.format_duration(delta.total_seconds())
        )
