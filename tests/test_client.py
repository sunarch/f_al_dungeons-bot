from src import client

# TODO: perhaps do proper mocking here for Mastodon

def test_post_status() -> None:

    client.dry = True

    assert "id" in client.post_status("Test status", "Last result")
    assert "id" in client.post_status("Test status", "Last result", False)


def test_post_poll() -> None:

    client.dry = True

    status = client.post_poll("Test status", "Last result", ["Option 1", "Option 2"])
    assert "id" in status
    assert "poll" in status
    assert client.status_chain == status["id"]

    status = client.post_poll("Test status", "Last result", ["Option 1", "Option 2"], False)
    assert "id" in status
    assert "poll" in status
    assert client.status_chain == status["id"]

    status = client.post_poll("Test status", "Last result", ["Option 1", "Option 2"], extend = True)
    assert "id" in status
    assert "poll" in status
    assert client.status_chain == status["id"]

    status = client.post_poll("Test status", "Last result", ["Option 1", "Option 2"], False, True)
    assert "id" in status
    assert "poll" in status
    assert client.status_chain == status["id"]


def test_delete_post() -> None:

    client.dry = True

    status = client.post_status("Test status", "Last result")
    assert client.delete_post(status["id"])


def test_poll_result() -> None:

    client.dry = True

    options = ["Option 1", "Option 2"]
    status = client.post_poll("Test status", "Last result", options)
    result = client.poll_result(status["id"])
    assert result >= 0 and result < len(options)


def test_get_followers() -> None:

    client.dry = True

    followers = client.get_followers()
    assert len(followers) > 0

    for f in followers:
        assert isinstance(f, str)
        assert len(f) > 0


def test_update_profile() -> None:

    client.dry = True

    assert "note" in client.update_profile("Bio")
