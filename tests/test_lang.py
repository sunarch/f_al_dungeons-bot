from src import lang

def test_globals() -> None:

    # Make sure data actually exists in every key
    def test_keys(d):
        assert len(d) > 0
        if (isinstance(d, dict)):
            for key in d:
                test_keys(d[key])
        else:
            assert isinstance(d, list)

    assert len(lang.lang) > 0
    test_keys(lang.lang)


def test_langitem() -> None:

    for key in lang.lang:
        assert lang.LangItem(lang.lang, key)
